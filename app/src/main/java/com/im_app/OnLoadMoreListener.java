package com.im_app;

public interface OnLoadMoreListener {
    void onLoadMore();
}