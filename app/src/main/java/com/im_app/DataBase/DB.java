package com.im_app.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.im_app.api.Comment;
import com.im_app.api.Feeling;
import com.im_app.api.Group;
import com.im_app.api.MainPage_;
import com.im_app.api.Memo;
import com.im_app.api.Person;
import com.im_app.api.Persons;
import com.im_app.api.User_;
import com.im_app.api.Users;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

public class DB extends DatabaseHandler {

    SQLiteDatabase db;

    public DB(Context c) {
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DB open() throws SQLDataException {
        db = getWritableDatabase();
        return this;
    }

    public void close() {
        db.close();
    }

    // FEELING
    // -------------------------------------------------------------------------------------------------------------------

    public Cursor getAllFEELING() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {colFEELING._ID,
                colFEELING.KEY_NAME,
                colFEELING.KEY_ICON,
                colFEELING.KEY_UPDATE_TIME};

        return db.query(TABLE_FEELING, columns, null, null, null, null, null);
    }

    public List<Feeling> getFEELING(int id) {
        List<Feeling> list = new ArrayList<>();

        String query = "SELECT " + colFEELING._ID + "," +
                colFEELING.KEY_NAME + "," +
                colFEELING.KEY_ICON + "," +
                colFEELING.KEY_UPDATE_TIME
                + " FROM " + TABLE_FEELING + " WHERE " + colFEELING._ID + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                Feeling offers = new Feeling(c.getInt(c.getColumnIndex(colFEELING._ID)),
                        c.getString(c.getColumnIndex(colFEELING.KEY_NAME)),
                        c.getString(c.getColumnIndex(colFEELING.KEY_ICON)),
                        c.getString(c.getColumnIndex(colFEELING.KEY_UPDATE_TIME)));
                list.add(offers);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    public List<Feeling> getFEELINGS() {
        List<Feeling> list = new ArrayList<>();

        String query = "SELECT " + colFEELING._ID + "," +
                colFEELING.KEY_NAME + "," +
                colFEELING.KEY_ICON + "," +
                colFEELING.KEY_UPDATE_TIME
                + " FROM " + TABLE_FEELING;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                Feeling feeling = new Feeling(c.getInt(c.getColumnIndex(colFEELING._ID)),
                        c.getString(c.getColumnIndex(colFEELING.KEY_NAME)),
                        c.getString(c.getColumnIndex(colFEELING.KEY_ICON)),
                        c.getString(c.getColumnIndex(colFEELING.KEY_UPDATE_TIME)));
                list.add(feeling);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    public Long insertFEELING(int id, String name, String icon, String updateAt) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colFEELING._ID, id);
        val.put(colFEELING.KEY_NAME, name);
        val.put(colFEELING.KEY_ICON, icon);
        val.put(colFEELING.KEY_UPDATE_TIME, updateAt);

        return db.insertOrThrow(TABLE_FEELING, null, val);
    }

    public boolean deleteFEELING(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_FEELING, colFEELING._ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public boolean updateFEELING(int id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colFEELING._ID, id);
        val.put(colFEELING.KEY_NAME, name);

        return db.update(TABLE_FEELING, val, colFEELING._ID + " = " + id, null) > 0;
    }

    public static abstract class colFEELING implements BaseColumns {
        public static final String KEY_NAME = "name";
        public static final String KEY_ICON = "icon";
        public static final String KEY_UPDATE_TIME = "update_time";
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // MEMO
    // -------------------------------------------------------------------------------------------------------------------

    public Cursor getAllMEMO() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {colMEMO._ID,
                colMEMO.KEY_TYPE_MEMO,
                colMEMO.KEY_USER_ID,
                colMEMO.KEY_USER_IMAGE,
                colMEMO.KEY_USER_NAME,
                colMEMO.KEY_MEMO_TITLE,
                colMEMO.KEY_MEMO_CONTENT,
                colMEMO.KEY_DATE_AND_TIME,
                colMEMO.KEY_TYPE,
                colMEMO.KEY_NUM_OF_LIKES,
                colMEMO.KEY_NUM_OF_COMMENTS,
                colMEMO.KEY_UPDATE_TIME};

        return db.query(TABLE_MEMO, columns, null, null, null, null, null);
    }

    public List<Memo> getAllMEMO(String type, int user_id) {
        List<Memo> list = new ArrayList<>();

        String query = "SELECT " + colMEMO._ID + "," +
                colMEMO.KEY_TYPE_MEMO + "," +
                colMEMO.KEY_USER_ID + "," +
                colMEMO.KEY_USER_IMAGE + "," +
                colMEMO.KEY_USER_NAME + "," +
                colMEMO.KEY_MEMO_TITLE + "," +
                colMEMO.KEY_MEMO_CONTENT + "," +
                colMEMO.KEY_DATE_AND_TIME + "," +
                colMEMO.KEY_TYPE + "," +
                colMEMO.KEY_NUM_OF_LIKES + "," +
                colMEMO.KEY_NUM_OF_COMMENTS + "," +
                colMEMO.KEY_UPDATE_TIME
                + " FROM " + TABLE_MEMO + " WHERE " + colMEMO.KEY_TYPE_MEMO + " = '" + type + "'"
                + " AND " + colMEMO._ID + " = " + user_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {

                Users users = new Users(c.getInt(c.getColumnIndex(colMEMO.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_USER_NAME)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_USER_IMAGE)));

                Memo memo = new Memo(c.getInt(c.getColumnIndex(colMEMO._ID)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_NUM_OF_COMMENTS)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_NUM_OF_LIKES)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_MEMO_TITLE)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_MEMO_CONTENT)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_TYPE)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_DATE_AND_TIME)),
                        users);
                list.add(memo);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    public List<MainPage_> getAllMAINPAGE(String type) {
        List<MainPage_> list = new ArrayList<>();

        String query = "SELECT " + colMEMO._ID + "," +
                colMEMO.KEY_TYPE_MEMO + "," +
                colMEMO.KEY_USER_ID + "," +
                colMEMO.KEY_USER_IMAGE + "," +
                colMEMO.KEY_USER_NAME + "," +
                colMEMO.KEY_MEMO_TITLE + "," +
                colMEMO.KEY_MEMO_CONTENT + "," +
                colMEMO.KEY_DATE_AND_TIME + "," +
                colMEMO.KEY_TYPE + "," +
                colMEMO.KEY_NUM_OF_LIKES + "," +
                colMEMO.KEY_NUM_OF_COMMENTS + "," +
                colMEMO.KEY_UPDATE_TIME
                + " FROM " + TABLE_MEMO + " WHERE " + colMEMO.KEY_TYPE_MEMO + " = '" + type + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {

                Users users = new Users(c.getInt(c.getColumnIndex(colMEMO.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_USER_NAME)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_USER_IMAGE)));

                MainPage_ memo = new MainPage_(c.getInt(c.getColumnIndex(colMEMO._ID)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_NUM_OF_COMMENTS)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_NUM_OF_LIKES)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_MEMO_TITLE)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_MEMO_CONTENT)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_TYPE)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_DATE_AND_TIME)),
                        users);
                list.add(memo);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    public Memo getMEMO(int id) {
        Memo memo = null;

        String query = "SELECT " + colMEMO._ID + "," +
                colMEMO.KEY_TYPE_MEMO + "," +
                colMEMO.KEY_USER_ID + "," +
                colMEMO.KEY_USER_IMAGE + "," +
                colMEMO.KEY_USER_NAME + "," +
                colMEMO.KEY_MEMO_TITLE + "," +
                colMEMO.KEY_MEMO_CONTENT + "," +
                colMEMO.KEY_DATE_AND_TIME + "," +
                colMEMO.KEY_TYPE + "," +
                colMEMO.KEY_NUM_OF_LIKES + "," +
                colMEMO.KEY_NUM_OF_COMMENTS + "," +
                colMEMO.KEY_UPDATE_TIME
                + " FROM " + TABLE_MEMO + " WHERE " + colMEMO._ID + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {

                Users users = new Users(c.getInt(c.getColumnIndex(colMEMO.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_USER_NAME)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_USER_IMAGE)));

                memo = new Memo(c.getInt(c.getColumnIndex(colMEMO._ID)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_NUM_OF_LIKES)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_NUM_OF_COMMENTS)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_MEMO_TITLE)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_MEMO_CONTENT)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colMEMO.KEY_DATE_AND_TIME)),
                        users);
//                list.add(offers);
            } while (c.moveToNext());
        }

        c.close();

        return memo;
    }

    public Long insertMEMO(int id, int user_id, String user_image, String user_name,
                           String memo_title, String memo_text, String date_and_time, String type, int num_of_likes,
                           int num_of_comments) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colMEMO._ID, id);
        val.put(colMEMO.KEY_USER_ID, user_id);
        val.put(colMEMO.KEY_USER_IMAGE, user_image);
        val.put(colMEMO.KEY_USER_NAME, user_name);
        val.put(colMEMO.KEY_MEMO_TITLE, memo_title);
        val.put(colMEMO.KEY_MEMO_CONTENT, memo_text);
        val.put(colMEMO.KEY_DATE_AND_TIME, date_and_time);
        val.put(colMEMO.KEY_TYPE, type);
        val.put(colMEMO.KEY_NUM_OF_LIKES, num_of_likes);
        val.put(colMEMO.KEY_NUM_OF_COMMENTS, num_of_comments);

        return db.insertOrThrow(TABLE_MEMO, null, val);
    }

    public Long insert_MAIN_PAGE(String type_memo, int id, int user_id, String user_image, String user_name,
                                 String memo_title, String memo_text, String date_and_time, String type, int num_of_likes,
                                 int num_of_comments) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colMEMO._ID, id);
        val.put(colMEMO.KEY_TYPE_MEMO, type_memo);
        val.put(colMEMO.KEY_USER_ID, user_id);
        val.put(colMEMO.KEY_USER_IMAGE, user_image);
        val.put(colMEMO.KEY_USER_NAME, user_name);
        val.put(colMEMO.KEY_MEMO_TITLE, memo_title);
        val.put(colMEMO.KEY_MEMO_CONTENT, memo_text);
        val.put(colMEMO.KEY_DATE_AND_TIME, date_and_time);
        val.put(colMEMO.KEY_TYPE, type);
        val.put(colMEMO.KEY_NUM_OF_LIKES, num_of_likes);
        val.put(colMEMO.KEY_NUM_OF_COMMENTS, num_of_comments);

        return db.insertOrThrow(TABLE_MEMO, null, val);
    }

    public boolean deleteMEMO(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_MEMO, colMEMO._ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public boolean updateMEMO(int id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colMEMO._ID, id);

        return db.update(TABLE_MEMO, val, colMEMO._ID + " = " + id, null) > 0;
    }

    public static abstract class colMEMO implements BaseColumns {
        public static final String KEY_TYPE_MEMO = "type_memo";
        public static final String KEY_USER_ID = "user_id";
        public static final String KEY_USER_IMAGE = "user_image";
        public static final String KEY_USER_NAME = "user_name";
        public static final String KEY_MEMO_TITLE = "memo_title";
        public static final String KEY_MEMO_CONTENT = "memo_content";
        public static final String KEY_DATE_AND_TIME = "date_and_time";
        public static final String KEY_TYPE = "type";
        public static final String KEY_NUM_OF_LIKES = "num_of_likes";
        public static final String KEY_NUM_OF_COMMENTS = "num_of_comments";
        public static final String KEY_UPDATE_TIME = "update_time";
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // USER
    // -------------------------------------------------------------------------------------------------------------------

    public Cursor getAllUSER() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {colUSER._ID,
                colUSER.KEY_IMAGE,
                colUSER.KEY_USER_NAME,
                colUSER.KEY_NAME,
                colUSER.KEY_EMAIL,
                colUSER.KEY_GENDER,
                colUSER.KEY_DOB};

        return db.query(TABLE_USER, columns, null, null, null, null, null);
    }

    public User_ getUSER(int id) {
        User_ user = null;
        String query = "SELECT " + colUSER._ID + "," +
                colUSER.KEY_NAME + "," +
                colUSER.KEY_USER_NAME + "," +
                colUSER.KEY_EMAIL + "," +
                colUSER.KEY_IMAGE + "," +
                colUSER.KEY_DOB + "," +
                colUSER.KEY_GENDER
                + " FROM " + TABLE_USER + " WHERE " + colUSER._ID + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                user = new User_(c.getInt(c.getColumnIndex(colUSER._ID)),
                        c.getString(c.getColumnIndex(colUSER.KEY_USER_NAME)),
                        c.getString(c.getColumnIndex(colUSER.KEY_EMAIL)),
                        c.getString(c.getColumnIndex(colUSER.KEY_NAME)),
                        c.getString(c.getColumnIndex(colUSER.KEY_DOB)),
                        c.getString(c.getColumnIndex(colUSER.KEY_GENDER)),
                        c.getString(c.getColumnIndex(colUSER.KEY_IMAGE)));
            } while (c.moveToNext());
        }

        c.close();

        return user;
    }

    public Long insertUSER(int id, String name, String user_name, String email, String dob, String gender,
                           String image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colUSER._ID, id);
        val.put(colUSER.KEY_NAME, name);
        val.put(colUSER.KEY_USER_NAME, user_name);
        val.put(colUSER.KEY_EMAIL, email);
        val.put(colUSER.KEY_DOB, dob);
        val.put(colUSER.KEY_GENDER, gender);
        val.put(colUSER.KEY_IMAGE, image);

        return db.insertOrThrow(TABLE_USER, null, val);
    }

    public boolean deleteUSER(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_USER, colUSER._ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public boolean updateUSER(int id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colUSER._ID, id);
        val.put(colUSER.KEY_NAME, name);

        return db.update(TABLE_USER, val, colUSER._ID + " = " + id, null) > 0;
    }

    public static abstract class colUSER implements BaseColumns {
        public static final String KEY_IMAGE = "image";
        public static final String KEY_USER_NAME = "user_name";
        public static final String KEY_NAME = "name";
        public static final String KEY_EMAIL = "email";
        public static final String KEY_GENDER = "gender";
        public static final String KEY_DOB = "dob";
//        public static final String KEY_UPDATE_TIME = "update_time";
//        public static final String KEY_MOBILE = "mobile";
//        public static final String KEY_TYPE = "type";
//        public static final String KEY_STATUS = "status";
//        public static final String KEY_RATE = "rate";
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // PEOPLE
    // -------------------------------------------------------------------------------------------------------------------

    public Cursor getAllPEOPLE() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {colPEOPLE._ID,
                colPEOPLE.KEY_USER_ID,
                colPEOPLE.KEY_PERSON_ID,
                colPEOPLE.KEY_PERSON_IMAGE,
                colPEOPLE.KEY_PERSON_USER_NAME,
                colPEOPLE.KEY_TYPE,
                colPEOPLE.KEY_UPDATE_TIME};

        return db.query(TABLE_PEOPLE, columns, null, null, null, null, null);
    }

    public List<Persons> getPEOPLE(int id, String type) {
        List<Persons> list = new ArrayList<>();

        String query = "SELECT " + colPEOPLE._ID + "," +
                colPEOPLE.KEY_USER_ID + "," +
                colPEOPLE.KEY_PERSON_ID + "," +
                colPEOPLE.KEY_PERSON_IMAGE + "," +
                colPEOPLE.KEY_PERSON_USER_NAME + "," +
                colPEOPLE.KEY_TYPE + "," +
                colPEOPLE.KEY_UPDATE_TIME
                + " FROM " + TABLE_PEOPLE + " WHERE " + colPEOPLE.KEY_USER_ID + " = " + id
                + " AND " + colPEOPLE.KEY_TYPE + " = '" + type + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                Person person = new Person(c.getInt(c.getColumnIndex(colPEOPLE.KEY_PERSON_ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_PERSON_USER_NAME)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_PERSON_IMAGE)));
                Users users = new Users(c.getInt(c.getColumnIndex(colPEOPLE.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_USER_ID)));
                Persons persons = new Persons(c.getInt(c.getColumnIndex(colPEOPLE._ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_UPDATE_TIME)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_TYPE)),
                        users, person);
                list.add(persons);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    public Persons getPEOPLE(int user_id, int person_id) {
        Persons persons = null;

        String query = "SELECT " + colPEOPLE._ID + "," +
                colPEOPLE.KEY_USER_ID + "," +
                colPEOPLE.KEY_PERSON_ID + "," +
                colPEOPLE.KEY_PERSON_IMAGE + "," +
                colPEOPLE.KEY_PERSON_USER_NAME + "," +
                colPEOPLE.KEY_TYPE + "," +
                colPEOPLE.KEY_UPDATE_TIME
                + " FROM " + TABLE_PEOPLE + " WHERE " + colPEOPLE.KEY_USER_ID + " = " + user_id
                + " AND " + colPEOPLE.KEY_PERSON_ID + " = " + person_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                Person person = new Person(c.getInt(c.getColumnIndex(colPEOPLE.KEY_PERSON_ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_PERSON_USER_NAME)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_PERSON_IMAGE)));
                Users users = new Users(c.getInt(c.getColumnIndex(colPEOPLE.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_USER_ID)));
                persons = new Persons(c.getInt(c.getColumnIndex(colPEOPLE._ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_UPDATE_TIME)),
                        c.getString(c.getColumnIndex(colPEOPLE.KEY_TYPE)),
                        users, person);
            } while (c.moveToNext());
        }

        c.close();

        return persons;
    }

    public Long insertPEOPLE(int id, int user_id, int person_id, String person_image, String person_user_name,
                             String type, String updateAt) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colPEOPLE._ID, id);
        val.put(colPEOPLE.KEY_USER_ID, user_id);
        val.put(colPEOPLE.KEY_PERSON_ID, person_id);
        val.put(colPEOPLE.KEY_PERSON_IMAGE, person_image);
        val.put(colPEOPLE.KEY_PERSON_USER_NAME, person_user_name);
        val.put(colPEOPLE.KEY_TYPE, type);
        val.put(colPEOPLE.KEY_UPDATE_TIME, updateAt);

        return db.insertOrThrow(TABLE_PEOPLE, null, val);
    }

    public boolean deletePEOPLE(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_PEOPLE, colPEOPLE._ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public boolean updatePEOPLE(int id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colPEOPLE._ID, id);

        return db.update(TABLE_PEOPLE, val, colPEOPLE._ID + " = " + id, null) > 0;
    }

    public static abstract class colPEOPLE implements BaseColumns {
        public static final String KEY_USER_ID = "user_id";
        public static final String KEY_PERSON_ID = "person_id";
        public static final String KEY_UPDATE_TIME = "update_time";
        public static final String KEY_PERSON_IMAGE = "person_image";
        public static final String KEY_PERSON_USER_NAME = "person_user_name";
        public static final String KEY_TYPE = "type";
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // COMMENTS
    // -------------------------------------------------------------------------------------------------------------------

    public Cursor getAllCOMMENTS() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {colCOMMENTS._ID,
                colCOMMENTS.KEY_USER_ID,
                colCOMMENTS.KEY_MEMO_ID,
                colCOMMENTS.KEY_USER_IMAGE,
                colCOMMENTS.KEY_USER_USER_NAME,
                colCOMMENTS.KEY_CREATED_AT,
                colCOMMENTS.KEY_COMMENT,
                colCOMMENTS.KEY_UPDATE_TIME};

        return db.query(TABLE_COMMENTS, columns, null, null, null, null, null);
    }

    public List<Comment> getCOMMENTS(int id) {
        List<Comment> list = new ArrayList<>();

        String query = "SELECT " + colCOMMENTS._ID + "," +
                colCOMMENTS.KEY_USER_ID + "," +
                colCOMMENTS.KEY_MEMO_ID + "," +
                colCOMMENTS.KEY_USER_IMAGE + "," +
                colCOMMENTS.KEY_USER_USER_NAME + "," +
                colCOMMENTS.KEY_CREATED_AT + "," +
                colCOMMENTS.KEY_COMMENT + "," +
                colCOMMENTS.KEY_UPDATE_TIME
                + " FROM " + TABLE_COMMENTS + " WHERE " + colCOMMENTS.KEY_MEMO_ID + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                int id_user = c.getInt(c.getColumnIndex(colCOMMENTS.KEY_USER_ID));
                String user_name = c.getString(c.getColumnIndex(colCOMMENTS.KEY_USER_USER_NAME));
                String user_img = c.getString(c.getColumnIndex(colCOMMENTS.KEY_USER_IMAGE));
                Users users = new Users(id_user, user_name, user_img);
                Comment comment = new Comment(c.getInt(c.getColumnIndex(colCOMMENTS._ID)),
                        c.getString(c.getColumnIndex(colCOMMENTS.KEY_MEMO_ID)),
                        c.getString(c.getColumnIndex(colCOMMENTS.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colCOMMENTS.KEY_COMMENT)),
                        c.getString(c.getColumnIndex(colCOMMENTS.KEY_CREATED_AT)),
                        users);
                list.add(comment);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    public Long insertCOMMENTS(int id, int user_id, int memo_id, String user_image, String user_name,
                               String created_at, String comment) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colCOMMENTS._ID, id);
        val.put(colCOMMENTS.KEY_USER_ID, user_id);
        val.put(colCOMMENTS.KEY_MEMO_ID, memo_id);
        val.put(colCOMMENTS.KEY_USER_IMAGE, user_image);
        val.put(colCOMMENTS.KEY_USER_USER_NAME, user_name);
        val.put(colCOMMENTS.KEY_CREATED_AT, created_at);
        val.put(colCOMMENTS.KEY_COMMENT, comment);

        return db.insertOrThrow(TABLE_COMMENTS, null, val);
    }

    public boolean deleteCOMMENTS(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_COMMENTS, colCOMMENTS._ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public boolean updateCOMMENTS(int id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colCOMMENTS._ID, id);

        return db.update(TABLE_COMMENTS, val, colCOMMENTS._ID + " = " + id, null) > 0;
    }

    public static abstract class colCOMMENTS implements BaseColumns {
        public static final String KEY_USER_ID = "user_id";
        public static final String KEY_UPDATE_TIME = "update_time";
        public static final String KEY_MEMO_ID = "memo_id";
        public static final String KEY_USER_IMAGE = "user_image";
        public static final String KEY_USER_USER_NAME = "user_name";
        public static final String KEY_CREATED_AT = "created_at";
        public static final String KEY_COMMENT = "comment";
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // GROUP
    // -------------------------------------------------------------------------------------------------------------------

//    public Cursor getAllGROUP() {
//        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = {colGROUP._ID,
//                colGROUP.KEY_NAME,
//                colGROUP.KEY_ADMIN_ID,
//                colGROUP.KEY_RATE,
//                colGROUP.KEY_TYPE,
//                colGROUP.KEY_STATUS,
//                colGROUP.KEY_UPDATE_TIME};
//
//        return db.query(TABLE_GROUP, columns, null, null, null, null, null);
//    }

    public List<Group> getAllGROUP() {
        List<Group> list = new ArrayList<>();
        String query = "SELECT " + colGROUP._ID + "," +
                colGROUP.KEY_ICON + "," +
                colGROUP.KEY_NAME + "," +
                colGROUP.KEY_ADMIN_ID + "," +
                colGROUP.KEY_NUMBER_OF_AVAILABLE + "," +
                colGROUP.KEY_STATUS + "," +
                colGROUP.KEY_UPDATE_TIME
                + " FROM " + TABLE_GROUP;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                Group group = new Group(c.getInt(c.getColumnIndex(colGROUP._ID)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_ADMIN_ID)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_ICON)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_NAME)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_STATUS)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_NUMBER_OF_AVAILABLE)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_UPDATE_TIME)));
                list.add(group);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    public List<Group> getGROUP(int id) {
        List<Group> list = new ArrayList<>();

        String query = "SELECT " + colGROUP._ID + "," +
                colGROUP.KEY_ICON + "," +
                colGROUP.KEY_NAME + "," +
                colGROUP.KEY_ADMIN_ID + "," +
                colGROUP.KEY_NUMBER_OF_AVAILABLE + "," +
                colGROUP.KEY_STATUS + "," +
                colGROUP.KEY_UPDATE_TIME
                + " FROM " + TABLE_GROUP + " WHERE " + colGROUP._ID + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                Group group = new Group(c.getInt(c.getColumnIndex(colGROUP._ID)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_ADMIN_ID)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_ICON)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_NAME)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_STATUS)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_NUMBER_OF_AVAILABLE)),
                        c.getString(c.getColumnIndex(colGROUP.KEY_UPDATE_TIME)));
                list.add(group);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    public Long insertGROUP(int id, String icon, String name, String adminId, String num_of_available, String status, String updateAt) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colGROUP._ID, id);
        val.put(colGROUP.KEY_ICON, icon);
        val.put(colGROUP.KEY_NAME, name);
        val.put(colGROUP.KEY_ADMIN_ID, adminId);
        val.put(colGROUP.KEY_NUMBER_OF_AVAILABLE, num_of_available);
        val.put(colGROUP.KEY_STATUS, status);
        val.put(colGROUP.KEY_UPDATE_TIME, updateAt);
        return db.insertOrThrow(TABLE_GROUP, null, val);
    }

    public boolean deleteGROUP(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_GROUP, colGROUP._ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public boolean updateGROUP(int id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colGROUP._ID, id);
        val.put(colGROUP.KEY_NAME, name);

        return db.update(TABLE_GROUP, val, colGROUP._ID + " = " + id, null) > 0;
    }

    public static abstract class colGROUP implements BaseColumns {
        public static final String KEY_ADMIN_ID = "admin_id";
        public static final String KEY_NAME = "name";
        public static final String KEY_STATUS = "status";
        public static final String KEY_NUMBER_OF_AVAILABLE = "number_of_available";
        public static final String KEY_UPDATE_TIME = "update_time";
        public static final String KEY_ICON = "icon";
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // ROOM_MESSAGES
    // -------------------------------------------------------------------------------------------------------------------

    public Cursor getAllROOM_MESSAGES() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {colROOM_MESSAGES._ID,
                colROOM_MESSAGES.KEY_USER_ID,
                colROOM_MESSAGES.KEY_USER_IMAGE,
                colROOM_MESSAGES.KEY_USER_USER_NAME,
                colROOM_MESSAGES.KEY_TEXT_MESSAGE,
                colROOM_MESSAGES.KEY_UPDATE_TIME};

        return db.query(TABLE_ROOM_MESSAGES, columns, null, null, null, null, null);
    }

    public List<String> getROOM_MESSAGES(int id) {
        List<String> list = new ArrayList<>();

        String query = "SELECT " + colROOM_MESSAGES._ID + "," +
                colROOM_MESSAGES.KEY_USER_ID + "," +
                colROOM_MESSAGES.KEY_USER_IMAGE + "," +
                colROOM_MESSAGES.KEY_USER_USER_NAME + "," +
                colROOM_MESSAGES.KEY_TEXT_MESSAGE + "," +
                colROOM_MESSAGES.KEY_UPDATE_TIME
                + " FROM " + TABLE_ROOM_MESSAGES + " WHERE " + colROOM_MESSAGES._ID + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
//                Offers offers = new Offers(c.getInt(c.getColumnIndex(colROOM_MESSAGES._ID)),
//                        c.getInt(c.getColumnIndex(colROOM_MESSAGES.KEY_COMPONY_ID)),
//                        c.getString(c.getColumnIndex(colROOM_MESSAGES.KEY_NAME)),
//                        c.getString(c.getColumnIndex(colROOM_MESSAGES.KEY_IMAGE)),
//                        c.getString(c.getColumnIndex(colROOM_MESSAGES.KEY_DESCRIBTION)));
//                list.add(offers);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    public Long insertROOM_MESSAGES(int id, int user_id, String user_image, String user_name, String message, String update_at) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colROOM_MESSAGES._ID, id);
        val.put(colROOM_MESSAGES.KEY_USER_ID, user_id);
        val.put(colROOM_MESSAGES.KEY_USER_IMAGE, user_image);
        val.put(colROOM_MESSAGES.KEY_USER_USER_NAME, user_name);
        val.put(colROOM_MESSAGES.KEY_TEXT_MESSAGE, message);
        val.put(colROOM_MESSAGES.KEY_UPDATE_TIME, update_at);
        return db.insertOrThrow(TABLE_ROOM_MESSAGES, null, val);
    }

    public boolean deleteROOM_MESSAGES(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_ROOM_MESSAGES, colROOM_MESSAGES._ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public boolean updateROOM_MESSAGES(int id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colROOM_MESSAGES._ID, id);
//        val.put(colROOM_MESSAGES.KEY_NAME, name);

        return db.update(TABLE_ROOM_MESSAGES, val, colROOM_MESSAGES._ID + " = " + id, null) > 0;
    }

    public static abstract class colROOM_MESSAGES implements BaseColumns {
        public static final String KEY_USER_ID = "user_id";
        public static final String KEY_TEXT_MESSAGE = "text_message";
        public static final String KEY_UPDATE_TIME = "update_time";
        public static final String KEY_USER_IMAGE = "user_image";
        public static final String KEY_USER_USER_NAME = "user_name";
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    // USER_MEMO
//    // -------------------------------------------------------------------------------------------------------------------
//
//    public Cursor getAllUSER_MEMO() {
//        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = {colUSER_MEMO._ID,
//                colUSER_MEMO.KEY_MEMO_ID,
//                colUSER_MEMO.KEY_USER_ID,
//                colUSER_MEMO.KEY_UPDATE_TIME};
//
//        return db.query(TABLE_USER_MEMO, columns, null, null, null, null, null);
//    }
//
////    public List<Offers> getUSER_MEMO(int id) {
////        List<Offers> list = new ArrayList<>();
////
////        String query = "SELECT " + colUSER_MEMO._ID + "," +
////                colUSER_MEMO.KEY_NAME + "," +
////                colUSER_MEMO.KEY_IMAGE + "," +
////                colUSER_MEMO.KEY_DESCRIBTION + "," +
////                colUSER_MEMO.KEY_COMPONY_ID
////                + " FROM " + TABLE_USER_MEMO + " WHERE " + colUSER_MEMO._ID + " = " + id;
////        SQLiteDatabase db = this.getReadableDatabase();
////        Cursor c = db.rawQuery(query, null);
////        if (c.moveToFirst()) {
////            do {
////                Offers offers = new Offers(c.getInt(c.getColumnIndex(colUSER_MEMO._ID)),
////                        c.getInt(c.getColumnIndex(colUSER_MEMO.KEY_COMPONY_ID)),
////                        c.getString(c.getColumnIndex(colUSER_MEMO.KEY_NAME)),
////                        c.getString(c.getColumnIndex(colUSER_MEMO.KEY_IMAGE)),
////                        c.getString(c.getColumnIndex(colUSER_MEMO.KEY_DESCRIBTION)));
////                list.add(offers);
////            } while (c.moveToNext());
////        }
////
////        c.close();
////
////        return list;
////    }
//
//    public Long insertUSER_MEMO(String name, String img, String describtion, int componyId) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues val = new ContentValues();
//
////        val.put(colUSER_MEMO.KEY_NAME, name);
////        val.put(colUSER_MEMO.KEY_IMAGE, img);
////        val.put(colUSER_MEMO.KEY_DESCRIBTION, describtion);
////        val.put(colUSER_MEMO.KEY_COMPONY_ID, componyId);
//
//        return db.insertOrThrow(TABLE_USER_MEMO, null, val);
//    }
//
//    public boolean deleteUSER_MEMO(int id) {
//        SQLiteDatabase db = getWritableDatabase();
//        return db.delete(TABLE_USER_MEMO, colUSER_MEMO._ID + "=?", new String[]{String.valueOf(id)}) > 0;
//    }
//
//    public boolean updateUSER_MEMO(int id, String name) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues val = new ContentValues();
//
//        val.put(colUSER_MEMO._ID, id);
//        val.put(colUSER_MEMO.KEY_USER_ID, name);
//
//        return db.update(TABLE_USER_MEMO, val, colUSER_MEMO._ID + " = " + id, null) > 0;
//    }
//
//    public static abstract class colUSER_MEMO implements BaseColumns {
//        public static final String KEY_USER_ID = "user_id";
//        public static final String KEY_MEMO_ID = "memo_id";
//        public static final String KEY_UPDATE_TIME = "update_time";
//    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // ROOM_USERS
    // -------------------------------------------------------------------------------------------------------------------

//    public Cursor getAllROOM_USERS() {
//        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = {colROOM_USERS._ID,
//                colROOM_USERS.KEY_NAME,
//                colROOM_USERS.KEY_IMAGE,
//                colROOM_USERS.KEY_DESCRIBTION,
//                colROOM_USERS.KEY_COMPONY_ID};
//
//        return db.query(TABLE_ROOM_USERS, columns, null, null, null, null, null);
//    }
//
//    public List<Offers> getROOM_USERS(int id) {
//        List<Offers> list = new ArrayList<>();
//
//        String query = "SELECT " + colROOM_USERS._ID + "," +
//                colROOM_USERS.KEY_NAME + "," +
//                colROOM_USERS.KEY_IMAGE + "," +
//                colROOM_USERS.KEY_DESCRIBTION + "," +
//                colROOM_USERS.KEY_COMPONY_ID
//                + " FROM " + TABLE_ROOM_USERS + " WHERE " + colROOM_USERS._ID + " = " + id;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor c = db.rawQuery(query, null);
//        if (c.moveToFirst()) {
//            do {
//                Offers offers = new Offers(c.getInt(c.getColumnIndex(colROOM_USERS._ID)),
//                        c.getInt(c.getColumnIndex(colROOM_USERS.KEY_COMPONY_ID)),
//                        c.getString(c.getColumnIndex(colROOM_USERS.KEY_NAME)),
//                        c.getString(c.getColumnIndex(colROOM_USERS.KEY_IMAGE)),
//                        c.getString(c.getColumnIndex(colROOM_USERS.KEY_DESCRIBTION)));
//                list.add(offers);
//            } while (c.moveToNext());
//        }
//
//        c.close();
//
//        return list;
//    }
//
//    public Long insertROOM_USERS(String name, String img, String describtion, int componyId) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues val = new ContentValues();
//
////        val.put(colROOM_USERS.KEY_NAME, name);
////        val.put(colROOM_USERS.KEY_IMAGE, img);
////        val.put(colROOM_USERS.KEY_DESCRIBTION, describtion);
////        val.put(colROOM_USERS.KEY_COMPONY_ID, componyId);
//
//        return db.insertOrThrow(TABLE_ROOM_USERS, null, val);
//    }
//
//    public boolean deleteROOM_USERS(int id) {
//        SQLiteDatabase db = getWritableDatabase();
//        return db.delete(TABLE_ROOM_USERS, colROOM_USERS._ID + "=?", new String[]{String.valueOf(id)}) > 0;
//    }
//
//    public boolean updateROOM_USERS(int id, String name) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues val = new ContentValues();
//
//        val.put(colROOM_USERS._ID, id);
////        val.put(colROOM_USERS.KEY_NAME, name);
//
//        return db.update(TABLE_ROOM_USERS, val, colROOM_USERS._ID + " = " + id, null) > 0;
//    }
//
//    public static abstract class colROOM_USERS implements BaseColumns {
//        public static final String KEY_ROOM_ID = "room_id";
//        public static final String KEY_USER_ID = "user_id";
//        public static final String KEY_UPDATE_TIME = "update_time";
//    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // ROOM
    // -------------------------------------------------------------------------------------------------------------------

//    public Cursor getAllROOM() {
//        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = {colROOM._ID,
//                colROOM.KEY_NAME,
//                colROOM.KEY_IMAGE,
//                colROOM.KEY_DESCRIBTION,
//                colROOM.KEY_COMPONY_ID};
//
//        return db.query(TABLE_ROOM, columns, null, null, null, null, null);
//    }
//
//    public List<Offers> getROOM(int id) {
//        List<Offers> list = new ArrayList<>();
//
//        String query = "SELECT " + colROOM._ID + "," +
//                colROOM.KEY_NAME + "," +
//                colROOM.KEY_IMAGE + "," +
//                colROOM.KEY_DESCRIBTION + "," +
//                colROOM.KEY_COMPONY_ID
//                + " FROM " + TABLE_ROOM + " WHERE " + colROOM._ID + " = " + id;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor c = db.rawQuery(query, null);
//        if (c.moveToFirst()) {
//            do {
//                Offers offers = new Offers(c.getInt(c.getColumnIndex(colROOM._ID)),
//                        c.getInt(c.getColumnIndex(colROOM.KEY_COMPONY_ID)),
//                        c.getString(c.getColumnIndex(colROOM.KEY_NAME)),
//                        c.getString(c.getColumnIndex(colROOM.KEY_IMAGE)),
//                        c.getString(c.getColumnIndex(colROOM.KEY_DESCRIBTION)));
//                list.add(offers);
//            } while (c.moveToNext());
//        }
//
//        c.close();
//
//        return list;
//    }
//
//    public Long insertROOM(String name, String img, String describtion, int componyId) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues val = new ContentValues();
//
//        val.put(colROOM.KEY_NAME, name);
////        val.put(colROOM.KEY_IMAGE, img);
////        val.put(colROOM.KEY_DESCRIBTION, describtion);
////        val.put(colROOM.KEY_COMPONY_ID, componyId);
//
//        return db.insertOrThrow(TABLE_ROOM, null, val);
//    }
//
//    public boolean deleteROOM(int id) {
//        SQLiteDatabase db = getWritableDatabase();
//        return db.delete(TABLE_ROOM, colROOM._ID + "=?", new String[]{String.valueOf(id)}) > 0;
//    }
//
//    public boolean updateROOM(int id, String name) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues val = new ContentValues();
//
//        val.put(colROOM._ID, id);
//        val.put(colROOM.KEY_NAME, name);
//
//        return db.update(TABLE_ROOM, val, colROOM._ID + " = " + id, null) > 0;
//    }
//
//    public static abstract class colROOM implements BaseColumns {
//        public static final String KEY_CATEGORY_ID = "category_id";
//        public static final String KEY_ADMIN_ID = "admin_id";
//        public static final String KEY_NAME = "name";
//        public static final String KEY_STATUS = "status";
//        public static final String KEY_UPDATE_TIME = "update_time";
//    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//
//    // MOTIVATION
//    // -------------------------------------------------------------------------------------------------------------------
//
//    public Cursor getAllMOTIVATION() {
//        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = {colMOTIVATION._ID,
//                colMOTIVATION.KEY_TYPE,
//                colMOTIVATION.KEY_TITLE,
//                colMOTIVATION.KEY_CONTENT,
//                colMOTIVATION.KEY_UPDATE_TIME};
//
//        return db.query(TABLE_MOTIVATION, columns, null, null, null, null, null);
//    }
//
//    public List<Motivation> getMOTIVATION(int id) {
//        List<Motivation> list = new ArrayList<>();
//
//        String query = "SELECT " + colMOTIVATION._ID + "," +
//                colMOTIVATION.KEY_TITLE + "," +
//                colMOTIVATION.KEY_CONTENT + "," +
//                colMOTIVATION.KEY_TYPE + "," +
//                colMOTIVATION.KEY_UPDATE_TIME
//                + " FROM " + TABLE_MOTIVATION + " WHERE " + colMOTIVATION._ID + " = " + id;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor c = db.rawQuery(query, null);
//        if (c.moveToFirst()) {
//            do {
//                Motivation offers = new Motivation(c.getInt(c.getColumnIndex(colMOTIVATION._ID)),
//                        c.getString(c.getColumnIndex(colMOTIVATION.KEY_TITLE)),
//                        c.getString(c.getColumnIndex(colMOTIVATION.KEY_CONTENT)),
//                        c.getString(c.getColumnIndex(colMOTIVATION.KEY_TYPE)),
//                        c.getString(c.getColumnIndex(colMOTIVATION.KEY_UPDATE_TIME)));
//                list.add(offers);
//            } while (c.moveToNext());
//        }
//
//        c.close();
//
//        return list;
//    }
//
//    public Long insertMOTIVATION(int id, String type, String title, String content, String updateAt) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues val = new ContentValues();
//
//        val.put(colMOTIVATION._ID, id);
//        val.put(colMOTIVATION.KEY_TYPE, type);
//        val.put(colMOTIVATION.KEY_TITLE, title);
//        val.put(colMOTIVATION.KEY_CONTENT, content);
//        val.put(colMOTIVATION.KEY_UPDATE_TIME, updateAt);
//
//        return db.insertOrThrow(TABLE_MOTIVATION, null, val);
//    }
//
//    public boolean deleteMOTIVATION(int id) {
//        SQLiteDatabase db = getWritableDatabase();
//        return db.delete(TABLE_MOTIVATION, colMOTIVATION._ID + "=?", new String[]{String.valueOf(id)}) > 0;
//    }
//
//    public boolean updateMOTIVATION(int id, String name) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues val = new ContentValues();
//
//        val.put(colMOTIVATION._ID, id);
//
//        return db.update(TABLE_MOTIVATION, val, colMOTIVATION._ID + " = " + id, null) > 0;
//    }
//
//    public static abstract class colMOTIVATION implements BaseColumns {
//        public static final String KEY_TYPE = "type";
//        public static final String KEY_TITLE = "title";
//        public static final String KEY_CONTENT = "content";
//        public static final String KEY_UPDATE_TIME = "update_time";
//    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public long count() {
        return DatabaseUtils.queryNumEntries(db, "TABLE_RECORD");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
