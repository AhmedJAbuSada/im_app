package com.im_app.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    // Database Version
    public static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "im";

    // tables name
    public static final String TABLE_USER = "user";
    public static final String TABLE_PEOPLE = "user_people";
    public static final String TABLE_MEMO = "memo";
    public static final String TABLE_FEELING = "feeling";
    public static final String TABLE_COMMENTS = "comments";
    public static final String TABLE_GROUP = "group_";
    public static final String TABLE_ROOM_MESSAGES = "room_messages";
//    public static final String TABLE_ROOM = "room";

    // --------------------------------------------------------
    public interface DbCallback {
        public void onCreate(SQLiteDatabase db);

        public void onUpgrade(SQLiteDatabase db);
    }

    Context context;

    public DatabaseHandler(Context context, String name, CursorFactory cursor,
                           int version) {
        super(context, DATABASE_NAME, cursor, DATABASE_VERSION);
        this.context = context;
    }

    // ---------------------------------------------------------
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_FEELING_TABLE = "CREATE TABLE " + TABLE_FEELING + "("
                + DB.colFEELING._ID + " INTEGER PRIMARY KEY ,"
                + DB.colFEELING.KEY_NAME + " VARCHAR(255) ,"
                + DB.colFEELING.KEY_ICON + " VARCHAR(255) ,"
                + DB.colFEELING.KEY_UPDATE_TIME + " DATE )";
        db.execSQL(CREATE_FEELING_TABLE);

        String CREATE_MEMO_TABLE = "CREATE TABLE " + TABLE_MEMO + "("
                + DB.colMEMO._ID + " INTEGER PRIMARY KEY,"
                + DB.colMEMO.KEY_USER_ID + " INTEGER,"
                + DB.colMEMO.KEY_USER_IMAGE + " VARCHAR(255),"
                + DB.colMEMO.KEY_USER_NAME + " VARCHAR(255),"
                + DB.colMEMO.KEY_MEMO_TITLE + " VARCHAR(255),"
                + DB.colMEMO.KEY_MEMO_CONTENT + " TEXT,"
                + DB.colMEMO.KEY_DATE_AND_TIME + " DATETIME,"
                + DB.colMEMO.KEY_TYPE + " VARCHAR(255),"
                + DB.colMEMO.KEY_TYPE_MEMO + " VARCHAR(255),"
                + DB.colMEMO.KEY_NUM_OF_LIKES + " INTEGER, "
                + DB.colMEMO.KEY_NUM_OF_COMMENTS + " INTEGER, "
                + DB.colMEMO.KEY_UPDATE_TIME + " DATE )";
        db.execSQL(CREATE_MEMO_TABLE);

        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + DB.colUSER._ID + " INTEGER PRIMARY KEY ,"
                + DB.colUSER.KEY_IMAGE + " VARCHAR(255) ,"
                + DB.colUSER.KEY_USER_NAME + " VARCHAR(255),"
                + DB.colUSER.KEY_NAME + " VARCHAR(255),"
                + DB.colUSER.KEY_EMAIL + " VARCHAR(255),"
                + DB.colUSER.KEY_DOB + " DATE ,"
                + DB.colUSER.KEY_GENDER + " VARCHAR(255))";
        db.execSQL(CREATE_USER_TABLE);

        String CREATE_PEOPLE_TABLE = "CREATE TABLE " + TABLE_PEOPLE + "("
                + DB.colPEOPLE._ID + " INTEGER PRIMARY KEY ,"
                + DB.colPEOPLE.KEY_USER_ID + " INTEGER,"
                + DB.colPEOPLE.KEY_PERSON_ID + " INTEGER,"
                + DB.colPEOPLE.KEY_PERSON_IMAGE + "  VARCHAR(255),"
                + DB.colPEOPLE.KEY_PERSON_USER_NAME + "  VARCHAR(255),"
                + DB.colPEOPLE.KEY_TYPE + "  VARCHAR(255),"
                + DB.colPEOPLE.KEY_UPDATE_TIME + " DATE )";
        db.execSQL(CREATE_PEOPLE_TABLE);

        String CREATE_COMMENTS_TABLE = "CREATE TABLE " + TABLE_COMMENTS + "("
                + DB.colCOMMENTS._ID + " INTEGER PRIMARY KEY ,"
                + DB.colCOMMENTS.KEY_MEMO_ID + " INTEGER,"
                + DB.colCOMMENTS.KEY_USER_ID + " INTEGER,"
                + DB.colCOMMENTS.KEY_USER_IMAGE + " VARCHAR(255),"
                + DB.colCOMMENTS.KEY_USER_USER_NAME + " VARCHAR(255),"
                + DB.colCOMMENTS.KEY_CREATED_AT + " DATETIME,"
                + DB.colCOMMENTS.KEY_COMMENT + " TEXT,"
                + DB.colCOMMENTS.KEY_UPDATE_TIME + " DATE )";
        db.execSQL(CREATE_COMMENTS_TABLE);

        String CREATE_GROUP_TABLE = "CREATE TABLE " + TABLE_GROUP + "("
                + DB.colGROUP._ID + " INTEGER PRIMARY KEY ,"
                + DB.colGROUP.KEY_ADMIN_ID + " INTEGER,"
                + DB.colGROUP.KEY_ICON + " VARCHAR(255),"
                + DB.colGROUP.KEY_NAME + " VARCHAR(255),"
                + DB.colGROUP.KEY_STATUS + " VARCHAR(255),"
                + DB.colGROUP.KEY_NUMBER_OF_AVAILABLE + " INTEGER,"
                + DB.colGROUP.KEY_UPDATE_TIME + " DATE );";
        db.execSQL(CREATE_GROUP_TABLE);

        String CREATE_ROOM_MESSAGES_TABLE = "CREATE TABLE " + TABLE_ROOM_MESSAGES + "("
                + DB.colROOM_MESSAGES._ID + " INTEGER PRIMARY KEY ,"
                + DB.colROOM_MESSAGES.KEY_USER_ID + " INTEGER,"
                + DB.colROOM_MESSAGES.KEY_USER_IMAGE + " VARCHAR(255),"
                + DB.colROOM_MESSAGES.KEY_USER_USER_NAME + " VARCHAR(255),"
                + DB.colROOM_MESSAGES.KEY_TEXT_MESSAGE + " TEXT,"
                + DB.colROOM_MESSAGES.KEY_UPDATE_TIME + " DATE )";
        db.execSQL(CREATE_ROOM_MESSAGES_TABLE);

//        String CREATE_ROOM_TABLE = "CREATE TABLE " + TABLE_ROOM + "("
//                + DB.colROOM._ID + " INTEGER PRIMARY KEY ,"
//                + DB.colROOM.KEY_CATEGORY_ID + " INTEGER REFERENCES " + TABLE_GROUP + "(" + DB.colGROUP._ID + " ), "
//                + DB.colROOM.KEY_ADMIN_ID + " INTEGER REFERENCES " + TABLE_USER + "(" + DB.colUSER._ID + " ), "
//                + DB.colROOM.KEY_NAME + " VARCHAR(255),"
//                + DB.colROOM.KEY_STATUS + " INTEGER,"
//                + DB.colROOM.KEY_UPDATE_TIME + " DATE )";
//        db.execSQL(CREATE_ROOM_TABLE);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PEOPLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FEELING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOM_MESSAGES);

        // Create tables again
        onCreate(db);
    }


}
