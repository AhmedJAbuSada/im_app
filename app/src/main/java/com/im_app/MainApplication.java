package com.im_app;

import android.app.Application;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.raizlabs.android.dbflow.config.FlowManager;

import org.androidannotations.annotations.EApplication;
import org.androidannotations.annotations.sharedpreferences.Pref;

@EApplication
public class MainApplication extends Application{

    public static Gson gson;

    @Pref
    public static MyPrefs_ myPrefs;

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);

        FirebaseMessaging.getInstance().subscribeToTopic("news");
        Log.e("FirebaseMessaging", "Subscribed to news topic");
        Log.e("FirebaseInstanceId", "InstanceID token: " + FirebaseInstanceId.getInstance().getToken());
    }

//    public static User loadUser() throws JSONException {
//        User user = gson.fromJson(myPrefs.user().get(), User.class);
//        if (user != null) {
//            return user;
//        } else
//            return user = new User("null", "null", "null", false);
//
//    }
//
//    public static void saveUser(User user) {
//        myPrefs.user().put(gson.toJson(user));
//    }
}
