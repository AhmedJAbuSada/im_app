package com.im_app;

import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref
public interface MyPrefs {

    @DefaultString("null")
    String user();

    @DefaultString("null")
    String locations();

    @DefaultString("null")
    String manServices();

    @DefaultString("null")
    String womanServices();

    @DefaultString("null")
    String gifPath();

    @DefaultString("null")
    String banner();

}