
package com.im_app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Memo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("num_of_comments")
    @Expose
    private String numOfComments;
    @SerializedName("num_of_liks")
    @Expose
    private String numOfLiks;
    @SerializedName("title")
    @Expose
    private Object title;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("privacy")
    @Expose
    private String privacy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("users")
    @Expose
    private Users users;

    public Memo(Integer id, String numOfComments, String numOfLiks, String title, String text, String userId,
                String createdAt, Users users) {
        this.id = id;
        this.numOfComments = numOfComments;
        this.numOfLiks = numOfLiks;
        this.title = title;
        this.text = text;
        this.userId = userId;
        this.createdAt = createdAt;
        this.users = users;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The numOfComments
     */
    public String getNumOfComments() {
        return numOfComments;
    }

    /**
     * 
     * @param numOfComments
     *     The num_of_comments
     */
    public void setNumOfComments(String numOfComments) {
        this.numOfComments = numOfComments;
    }

    /**
     * 
     * @return
     *     The numOfLiks
     */
    public String getNumOfLiks() {
        return numOfLiks;
    }

    /**
     * 
     * @param numOfLiks
     *     The num_of_liks
     */
    public void setNumOfLiks(String numOfLiks) {
        this.numOfLiks = numOfLiks;
    }

    /**
     * 
     * @return
     *     The title
     */
    public Object getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(Object title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The text
     */
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The privacy
     */
    public String getPrivacy() {
        return privacy;
    }

    /**
     * 
     * @param privacy
     *     The privacy
     */
    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The users
     */
    public Users getUsers() {
        return users;
    }

    /**
     * 
     * @param users
     *     The users
     */
    public void setUsers(Users users) {
        this.users = users;
    }

}
