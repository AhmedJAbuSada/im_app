
package com.im_app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Feelings {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("feeling")
    @Expose
    private List<Feeling> feeling = new ArrayList<Feeling>();

    /**
     * 
     * @return
     *     The response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The feeling
     */
    public List<Feeling> getFeeling() {
        return feeling;
    }

    /**
     * 
     * @param feeling
     *     The feeling
     */
    public void setFeeling(List<Feeling> feeling) {
        this.feeling = feeling;
    }

}
