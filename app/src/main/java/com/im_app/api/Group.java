
package com.im_app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Group {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("admin_id")
    @Expose
    private String adminId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("num_of_members")
    @Expose
    private String numOfMembers;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Group(Integer id, String adminId, String name, String icon, String status, String numOfMembers, String createdAt) {
        this.id = id;
        this.adminId = adminId;
        this.name = name;
        this.icon = icon;
        this.status = status;
        this.numOfMembers = numOfMembers;
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The adminId
     */
    public String getAdminId() {
        return adminId;
    }

    /**
     * 
     * @param adminId
     *     The admin_id
     */
    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 
     * @param icon
     *     The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The numOfMembers
     */
    public String getNumOfMembers() {
        return numOfMembers;
    }

    /**
     * 
     * @param numOfMembers
     *     The num_of_members
     */
    public void setNumOfMembers(String numOfMembers) {
        this.numOfMembers = numOfMembers;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
