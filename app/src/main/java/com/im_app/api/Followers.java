
package com.im_app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Followers {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("userfollower")
    @Expose
    private List<Userfollower> userfollower = new ArrayList<Userfollower>();

    /**
     * 
     * @return
     *     The response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The userfollower
     */
    public List<Userfollower> getUserfollower() {
        return userfollower;
    }

    /**
     * 
     * @param userfollower
     *     The userfollower
     */
    public void setUserfollower(List<Userfollower> userfollower) {
        this.userfollower = userfollower;
    }

}
