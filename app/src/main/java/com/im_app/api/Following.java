
package com.im_app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Following {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("userfollowing")
    @Expose
    private List<Userfollowing> userfollowing = new ArrayList<Userfollowing>();

    /**
     * 
     * @return
     *     The response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The userfollowing
     */
    public List<Userfollowing> getUserfollowing() {
        return userfollowing;
    }

    /**
     * 
     * @param userfollowing
     *     The userfollowing
     */
    public void setUserfollowing(List<Userfollowing> userfollowing) {
        this.userfollowing = userfollowing;
    }

}
