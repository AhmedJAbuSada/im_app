
package com.im_app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class People {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("peoples")
    @Expose
    private List<Persons> peoples = new ArrayList<Persons>();

    /**
     * 
     * @return
     *     The response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The peoples
     */
    public List<Persons> getPeoples() {
        return peoples;
    }

    /**
     * 
     * @param peoples
     *     The peoples
     */
    public void setPeoples(List<Persons> peoples) {
        this.peoples = peoples;
    }

}
