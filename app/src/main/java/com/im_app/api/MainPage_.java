
package com.im_app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainPage_ {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("num_of_comments")
    @Expose
    private String numOfComments;
    @SerializedName("num_of_liks")
    @Expose
    private String numOfLiks;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("privacy")
    @Expose
    private Object privacy;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("users")
    @Expose
    private Users users;

    public MainPage_(Integer id, String numOfComments, String numOfLiks, String title, String content, String type,
                     String createdAt, Users users) {
        this.id = id;
        this.numOfComments = numOfComments;
        this.numOfLiks = numOfLiks;
        this.title = title;
        this.content = content;
        this.type = type;
        this.createdAt = createdAt;
        this.users = users;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The numOfComments
     */
    public String getNumOfComments() {
        return numOfComments;
    }

    /**
     * 
     * @param numOfComments
     *     The num_of_comments
     */
    public void setNumOfComments(String numOfComments) {
        this.numOfComments = numOfComments;
    }

    /**
     * 
     * @return
     *     The numOfLiks
     */
    public String getNumOfLiks() {
        return numOfLiks;
    }

    /**
     * 
     * @param numOfLiks
     *     The num_of_liks
     */
    public void setNumOfLiks(String numOfLiks) {
        this.numOfLiks = numOfLiks;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The content
     */
    public String getContent() {
        return content;
    }

    /**
     * 
     * @param content
     *     The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 
     * @return
     *     The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The privacy
     */
    public Object getPrivacy() {
        return privacy;
    }

    /**
     * 
     * @param privacy
     *     The privacy
     */
    public void setPrivacy(Object privacy) {
        this.privacy = privacy;
    }

    /**
     * 
     * @return
     *     The categoryId
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 
     * @param categoryId
     *     The category_id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The users
     */
    public Users getUsers() {
        return users;
    }

    /**
     * 
     * @param users
     *     The users
     */
    public void setUsers(Users users) {
        this.users = users;
    }

}
