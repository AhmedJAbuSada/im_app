
package com.im_app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Memos {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("memos")
    @Expose
    private List<Memo> memos = new ArrayList<Memo>();

    /**
     * 
     * @return
     *     The response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The memos
     */
    public List<Memo> getMemos() {
        return memos;
    }

    /**
     * 
     * @param memos
     *     The memos
     */
    public void setMemos(List<Memo> memos) {
        this.memos = memos;
    }

}
