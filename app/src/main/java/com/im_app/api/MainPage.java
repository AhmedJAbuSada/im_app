
package com.im_app.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MainPage {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("main_page")
    @Expose
    private List<MainPage_> mainPage = new ArrayList<MainPage_>();

    /**
     * 
     * @return
     *     The response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The mainPage
     */
    public List<MainPage_> getMainPage() {
        return mainPage;
    }

    /**
     * 
     * @param mainPage
     *     The main_page
     */
    public void setMainPage(List<MainPage_> mainPage) {
        this.mainPage = mainPage;
    }

}
