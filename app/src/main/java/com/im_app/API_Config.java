package com.im_app;

public class API_Config {
    public static final String URL_BASE = "http://smart-solution.co/iam";
    public static final String URL_PEOPLE = URL_BASE + "/json/peoples";
    public static final String URL_ADD_MEMO = URL_BASE + "/json/memo";
    public static final String URL_GET_MEMO = URL_BASE + "/json/memo";
    public static final String URL_CHANGE_PASSWORD = URL_BASE + "/json/change_password";
    public static final String URL_FOLLOWERS = URL_BASE + "/json/friends/followers";
    public static final String URL_FOLLOWING = URL_BASE + "/json/friends/following";
    public static final String URL_FOLLOW = URL_BASE + "/json/friends/follow/";
    public static final String URL_UNFOLLOW = URL_BASE + "/json/friends/unfollow/";
    public static final String URL_EDIT_PROFILE = URL_BASE + "/json/edit_profile";
    public static final String URL_GROUPS = URL_BASE + "/json/groups";
    public static final String URL_SIGN_IN = URL_BASE + "/json/login";
    public static final String URL_SIGN_UP = URL_BASE + "/json/signup";
    public static final String URL_MAIN_PAGE = URL_BASE + "/json/main_page";
    public static final String URL_LOGOUT = URL_BASE + "/json/logout";
    public static final String URL_FEELING = URL_BASE + "/json/feeling";
    public static final String URL_ADD_COMMENT = URL_BASE + "/json/comment";
    public static final String URL_GET_COMMENT = URL_BASE + "/json/comments/";
}
