package com.im_app.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.twitter.TwitterSocialNetwork;
import com.im_app.DataBase.DB;
import com.im_app.R;
import com.im_app.api.User_;

import org.json.JSONObject;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fragment_SignUp extends Fragment implements View.OnClickListener,
        SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener {
    private static final String TAG = "Fragment_SignUp";
    private static final String URL = "http://smart-solution.co/iam/json/signup";
    ProgressDialog pDialog;
    LinearLayout linearSend, linearFooter, linearVia, linearSocial;
    EditText name, userName, email, password;
    ImageView facebook, googlePlus, twitter;
    RadioButton male, female;
    Button signUp;
    List<View> listView = new ArrayList<>();
    DB db;
    public static SocialNetworkManager mSocialNetworkManager;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        db.close();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DB(getActivity());
        try {
            db.open();
        } catch (SQLDataException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("please wait");

//        if (checkPlayServices()) {
//            // Start IntentService to register this application with GCM.
//            Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
//            getActivity().startService(intent);
//        }

        findViews(view);

        return view;
    }

    private void findViews(View view) {
        name = (EditText) view.findViewById(R.id.name);
        userName = (EditText) view.findViewById(R.id.user_name);
        email = (EditText) view.findViewById(R.id.email);
        password = (EditText) view.findViewById(R.id.password);
        male = (RadioButton) view.findViewById(R.id.male);
        female = (RadioButton) view.findViewById(R.id.female);
        linearSend = (LinearLayout) view.findViewById(R.id.linear_send);
        signUp = (Button) linearSend.findViewById(R.id.send);
        linearFooter = (LinearLayout) view.findViewById(R.id.linear_footer);
        linearVia = (LinearLayout) linearFooter.findViewById(R.id.linear_via);
        linearSocial = (LinearLayout) linearVia.findViewById(R.id.linear_social);
        facebook = (ImageView) linearSocial.findViewById(R.id.facebook);
        googlePlus = (ImageView) linearSocial.findViewById(R.id.google_plus);
        twitter = (ImageView) linearSocial.findViewById(R.id.twitter);


//        listView.add(pic);
        listView.add(name);
        listView.add(userName);
        listView.add(email);
        listView.add(password);
        emptyText(listView);

        signUp.setOnClickListener(this);
        facebook.setOnClickListener(loginClick);
        googlePlus.setOnClickListener(loginClick);
        twitter.setOnClickListener(loginClick);


        ArrayList<String> fbScope = new ArrayList<>();
        fbScope.addAll(Arrays.asList("public_profile, email, user_friends"));
        //Use manager to manage SocialNetworks
        mSocialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(LoginActivity.SOCIAL_NETWORK_TAG);

        //Check if manager exist
        if (mSocialNetworkManager == null) {
            mSocialNetworkManager = new SocialNetworkManager();

            //Init and add to manager FacebookSocialNetwork
            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(this, fbScope);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);

            //Init and add to manager LinkedInSocialNetwork
            GooglePlusSocialNetwork gpNetwork = new GooglePlusSocialNetwork(this);
            mSocialNetworkManager.addSocialNetwork(gpNetwork);

            //Initiate every network from mSocialNetworkManager
            getFragmentManager().beginTransaction().add(mSocialNetworkManager, LoginActivity.SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            //if manager exist - get and setup login only for initialized SocialNetworks
            if (!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {
                    socialNetwork.setOnLoginCompleteListener(this);
                    initSocialNetwork(socialNetwork);
                }
            }
        }
    }

    private void initSocialNetwork(SocialNetwork socialNetwork) {
        if (socialNetwork.isConnected()) {
            switch (socialNetwork.getID()) {
                case FacebookSocialNetwork.ID:
//                    facebook.setText("Show Facebook profile");
                    break;
                case GooglePlusSocialNetwork.ID:
//                    googlePlus.setText("Show GooglePlus profile");
                    break;
            }
        }
    }

    @Override
    public void onSocialNetworkManagerInitialized() {
        //when init SocialNetworks - get and setup login only for initialized SocialNetworks
        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            socialNetwork.setOnLoginCompleteListener(this);
            initSocialNetwork(socialNetwork);
        }
    }

    private View.OnClickListener loginClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int networkId = 0;
            switch (view.getId()) {
                case R.id.facebook:
                    networkId = FacebookSocialNetwork.ID;
                    break;
                case R.id.google_plus:
                    networkId = GooglePlusSocialNetwork.ID;
                    break;
            }
            SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
            if (!socialNetwork.isConnected()) {
                if (networkId != 0) {
                    socialNetwork.requestLogin();
//                    MainActivity.showProgress("Loading social person");
                } else {
                    Toast.makeText(getActivity(), "Wrong networkId", Toast.LENGTH_LONG).show();
                }
            } else {
                startProfile(socialNetwork.getID());
            }
        }
    };

    @Override
    public void onLoginSuccess(int networkId) {
//        MainActivity.hideProgress();
        startProfile(networkId);
    }

    @Override
    public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
        Toast.makeText(getActivity(), "ERROR: " + errorMessage, Toast.LENGTH_LONG).show();
    }

    private void startProfile(int networkId) {
        CompleteSignUp profile = CompleteSignUp.newInstannce(networkId);
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack("profile")
                .replace(R.id.container, profile)
                .commit();
    }

    @Override
    public void onStop() {
        super.onStop();
        emptyText(listView);
    }

    @Override
    public void onClick(View v) {
        if (v == signUp) {
            // Handle clicks for button
//            getFromAPI();
            String na = name.getText().toString();
            String userN = userName.getText().toString();
            String e = email.getText().toString();
            String pass = password.getText().toString();
            String gender = "male";
            if (female.isChecked()) {
                gender = "female";
            }
            if (na.length() > 0 && userN.length() > 0 && e.length() > 0 && pass.length() > 0) {

                postSignUp(na, userN, e, pass, gender);
            } else {
                Toast.makeText(getActivity(), getString(R.string.fields), Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void postSignUp(final String name, final String userName, final String email, final String password, String gender) {
        final Map<String, String> params = new HashMap<>();
        try {
            params.put("name", name);
            params.put("username", userName);
            params.put("email", email);
            params.put("password", password);
            params.put("gender", gender);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String sentToken = sharedPreferences.getString(Fragment_SignIn.GCM_REGISTRATION_ID, "null");
            params.put(Fragment_SignIn.GCM_REGISTRATION_ID, sentToken);
        } catch (Exception e) {
            e.getMessage();
        }
        showpDialog();
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL, new JSONObject(params)
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.w("RES", response + "");
                    Log.e("in", "");
                    Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();
                    Log.w("RESUCCESS", response + "");
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equals("success")) {
                        JSONObject user = response.getJSONObject("user");
                        int id = user.getInt("id");
                        String username = user.getString("username");
                        String email = user.getString("email");
                        String name = user.getString("name");
                        String dob = user.getString("dob");
                        String gender = user.getString("gender");
                        String image = user.getString("image");
                        String token = user.getString("access_token");
                        User_ user1 = new User_(id, username, email, name, dob, gender, image);

                        db.deleteUSER(user1.getId());
                        db.insertUSER(user1.getId(), user1.getName(), user1.getUsername(), user1.getEmail(), user1.getDob(),
                                user1.getGender(), user1.getImage());

                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(Fragment_SignIn.MY_PREFS_NAME,
                                Context.MODE_PRIVATE).edit();
                        editor.putInt(Fragment_SignIn.USER_ID, user1.getId());
                        editor.putString(Fragment_SignIn.ACCESS_TOKEN, token);
                        editor.apply();

                        hidepDialog();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    } else
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    hidepDialog();
                } catch (Exception e) {
                    Log.e("out", "");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("first ERROR", error.getMessage() + ":");
                error.printStackTrace();
                Log.e("error", "");
                hidepDialog();
                Toast.makeText(getActivity(), "invalid", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        MySingleton.getInstance(getActivity()).addToRequestQueue(customRequest);
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void emptyText(List<?> editTexts) {
        for (int i = 0; i < editTexts.size(); i++) {
            if (editTexts.get(i) instanceof RadioButton || editTexts.get(i) instanceof CheckBox) {
                if (editTexts.get(i) instanceof RadioButton) {
                    ((RadioButton) editTexts.get(i)).setChecked(false);
                } else if (editTexts.get(i) instanceof Spinner) {
                    ((Spinner) editTexts.get(i)).setEnabled(false);
                } else if (editTexts.get(i) instanceof CheckBox) {
                    ((CheckBox) editTexts.get(i)).setChecked(false);
                }
            } else if (editTexts.get(i) instanceof EditText || editTexts.get(i) instanceof TextView) {
                if (editTexts.get(i) instanceof EditText) {
                    ((EditText) editTexts.get(i)).setText("");
                } else if (editTexts.get(i) instanceof TextView) {
                    ((TextView) editTexts.get(i)).setText("");
                }
            } else if (editTexts.get(i) instanceof ImageView) {
                ((ImageView) editTexts.get(i)).setImageDrawable(null);
            }
        }
    }

}
