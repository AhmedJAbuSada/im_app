package com.im_app.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.googleplus.GooglePlusSocialNetwork;
import com.github.gorbin.asne.twitter.TwitterSocialNetwork;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.im_app.DataBase.DB;
import com.im_app.api.User_;

import org.json.JSONObject;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fragment_SignIn extends Fragment implements View.OnClickListener,
        SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener {
    static final String MY_PREFS_NAME = "my_prefs_name";
    static final String USER_ID = "user_id";
    static final String ACCESS_TOKEN = "access_token";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String TAG = "Fragment_SignIn";
    public static final String GCM_REGISTRATION_ID = "gcm_registration_id";
    private static final String URL_ = "http://smart-solution.co/iam/json/login";
    EditText email, password;
    TextView forgetPassword;
    LinearLayout linearSend, linearFooter, linearVia, linearSocial;
    ImageView facebook, googlePlus, twitter;
    Button signIn;
    private ProgressDialog pDialog;
    DB db;
    public static SocialNetworkManager mSocialNetworkManager;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        db.close();
    }

    private void findViews(View view) {
        email = (EditText) view.findViewById(R.id.email);
        password = (EditText) view.findViewById(R.id.password);
        linearSend = (LinearLayout) view.findViewById(R.id.linear_send);
        signIn = (Button) linearSend.findViewById(R.id.send);
        forgetPassword = (TextView) view.findViewById(R.id.forgetPassword);
        linearFooter = (LinearLayout) view.findViewById(R.id.linear_footer);
        linearVia = (LinearLayout) linearFooter.findViewById(R.id.linear_via);
        linearSocial = (LinearLayout) linearVia.findViewById(R.id.linear_social);
        facebook = (ImageView) view.findViewById(R.id.facebook);
        googlePlus = (ImageView) view.findViewById(R.id.google_plus);
        twitter = (ImageView) view.findViewById(R.id.twitter);

        signIn.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
        facebook.setOnClickListener(loginClick);
        googlePlus.setOnClickListener(loginClick);
        twitter.setOnClickListener(loginClick);

        String TWITTER_CONSUMER_KEY = getActivity().getString(R.string.twitter_consumer_key);
        String TWITTER_CONSUMER_SECRET = getActivity().getString(R.string.twitter_consumer_secret);
        String TWITTER_CALLBACK_URL = "oauth://ASNE";

        ArrayList<String> fbScope = new ArrayList<>();
        fbScope.addAll(Arrays.asList("public_profile, email, user_friends"));
        String linkedInScope = "r_basicprofile+r_fullprofile+rw_nus+r_network+w_messages+r_emailaddress+r_contactinfo";

        //Use manager to manage SocialNetworks
        mSocialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(LoginActivity.SOCIAL_NETWORK_TAG);

        //Check if manager exist
        if (mSocialNetworkManager == null) {
            mSocialNetworkManager = new SocialNetworkManager();

            //Init and add to manager FacebookSocialNetwork
            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(this, fbScope);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);

            //Init and add to manager TwitterSocialNetwork
            TwitterSocialNetwork twNetwork = new TwitterSocialNetwork(this, TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_CALLBACK_URL);
            mSocialNetworkManager.addSocialNetwork(twNetwork);

            //Init and add to manager LinkedInSocialNetwork
            GooglePlusSocialNetwork gpNetwork = new GooglePlusSocialNetwork(this);
            mSocialNetworkManager.addSocialNetwork(gpNetwork);

            //Initiate every network from mSocialNetworkManager
            getFragmentManager().beginTransaction().add(mSocialNetworkManager, LoginActivity.SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            //if manager exist - get and setup login only for initialized SocialNetworks
            if (!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {
                    socialNetwork.setOnLoginCompleteListener(this);
                    initSocialNetwork(socialNetwork);
                }
            }
        }
    }

    private void initSocialNetwork(SocialNetwork socialNetwork) {
        if (socialNetwork.isConnected()) {
            switch (socialNetwork.getID()) {
                case FacebookSocialNetwork.ID:
//                    facebook.setText("Show Facebook profile");
                    break;
                case TwitterSocialNetwork.ID:
//                    twitter.setText("Show Twitter profile");
                    break;
                case GooglePlusSocialNetwork.ID:
//                    googlePlus.setText("Show GooglePlus profile");
                    break;
            }
        }
    }

    @Override
    public void onSocialNetworkManagerInitialized() {
        //when init SocialNetworks - get and setup login only for initialized SocialNetworks
        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            socialNetwork.setOnLoginCompleteListener(this);
            initSocialNetwork(socialNetwork);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == signIn) {
            // Handle clicks for signIn
            String e = email.getText().toString();
            String p = password.getText().toString();
            if (e.length() > 0 && p.length() > 0)
                postSignIn(e, p);
            else
                Toast.makeText(getActivity(), getString(R.string.fields), Toast.LENGTH_SHORT).show();
        }

        if (v == forgetPassword) {
            DialogFragment_ForgetPassword fragment = new DialogFragment_ForgetPassword();
            fragment.show(getFragmentManager(), "blur_sample");
        }
    }

    private View.OnClickListener loginClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int networkId = 0;
            switch (view.getId()) {
                case R.id.facebook:
                    networkId = FacebookSocialNetwork.ID;
                    break;
                case R.id.twitter:
                    networkId = TwitterSocialNetwork.ID;
                    break;
                case R.id.google_plus:
                    networkId = GooglePlusSocialNetwork.ID;
                    break;
            }
            SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
            if (!socialNetwork.isConnected()) {
                if (networkId != 0) {
                    socialNetwork.requestLogin();
//                    MainActivity.showProgress("Loading social person");
                } else {
                    Toast.makeText(getActivity(), "Wrong networkId", Toast.LENGTH_LONG).show();
                }
            } else {
                startProfile(socialNetwork.getID());
            }
        }
    };

    @Override
    public void onLoginSuccess(int networkId) {
//        MainActivity.hideProgress();
        startProfile(networkId);
    }

    @Override
    public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
        Toast.makeText(getActivity(), "ERROR: " + errorMessage, Toast.LENGTH_LONG).show();
    }

    private void startProfile(int networkId) {
        CompleteSignUp profile = CompleteSignUp.newInstannce(networkId);
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack("profile")
                .replace(R.id.container, profile)
                .commit();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DB(getActivity());
        try {
            db.open();
        } catch (SQLDataException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("please wait");

//        if (checkPlayServices()) {
//            // Start IntentService to register this application with GCM.
//            Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
//            getActivity().startService(intent);
//        }

        findViews(view);
        return view;
    }

    public void postSignIn(final String userName, final String password) {
        final Map<String, String> params = new HashMap<>();
        try {
            params.put("username", userName);
            params.put("password", password);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String sentToken = sharedPreferences.getString(GCM_REGISTRATION_ID, "null");
            params.put(GCM_REGISTRATION_ID, sentToken);
        } catch (Exception e) {
            e.getMessage();
        }
        showpDialog();
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL_, new JSONObject(params)
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("RES", response + "");
                    Log.e("in", "");
                    Log.w("RESUCCESS", response + "");
                    JSONObject responses = response.getJSONObject("response");
                    String status = responses.getString("status");
                    String message = responses.getString("message");

                    if (status.equals("success")) {
                        JSONObject user = response.getJSONObject("user");
                        int id = user.getInt("id");
                        String username = user.getString("username");
                        String email = user.getString("email");
                        String name = user.getString("name");
                        String dob = user.getString("dob");
                        String gender = user.getString("gender");
                        String image = user.getString("image");
                        String token = user.getString("access_token");
                        User_ user1 = new User_(id, username, email, name, dob, gender, image);

                        db.deleteUSER(user1.getId());
                        db.insertUSER(user1.getId(), user1.getName(), user1.getUsername(), user1.getEmail(), user1.getDob(),
                                user1.getGender(), user1.getImage());

                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME,
                                Context.MODE_PRIVATE).edit();
                        editor.putInt(USER_ID, user1.getId());
                        editor.putString(ACCESS_TOKEN, token);
                        editor.apply();

                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    } else
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    hidepDialog();
                } catch (Exception e) {
                    Log.e("out", "");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.getMessage());
                error.printStackTrace();
                hidepDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        MySingleton.getInstance(getActivity()).addToRequestQueue(customRequest);
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(getActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                getActivity().finish();
            }
            return false;
        }
        return true;
    }
}
