package com.im_app.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageLoader;
import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.listener.OnRequestSocialPersonCompleteListener;
import com.github.gorbin.asne.core.persons.SocialPerson;
import com.im_app.DataBase.DB;
import com.im_app.RoundedNetImage;
import com.im_app.api.User_;
import com.im_app.R;

import org.json.JSONObject;

import java.sql.SQLDataException;
import java.util.HashMap;
import java.util.Map;

// Created by Ahmed J AbuSada on 25/06/2016.
public class CompleteSignUp extends Fragment implements View.OnClickListener, OnRequestSocialPersonCompleteListener {
    private static final String NETWORK_ID = "NETWORK_ID";
    private static final String URL = "http://smart-solution.co/iam/json/signup";
    SocialNetwork socialNetwork;
    int networkId;
    String img;
    LinearLayout root, linearSend;
    FrameLayout frameImg;
    RoundedNetImage pic;
    EditText name, userName, email, password;
    RadioButton male, female;
    Button send;
    ProgressDialog pDialog;
    DB db;

    public static CompleteSignUp newInstannce(int id) {
        CompleteSignUp fragment = new CompleteSignUp();
        Bundle args = new Bundle();
        args.putInt(NETWORK_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    private void findViews(View view) {
        networkId = getArguments().containsKey(NETWORK_ID) ? getArguments().getInt(NETWORK_ID) : 0;
        root = (LinearLayout) view.findViewById(R.id.root);
        frameImg = (FrameLayout) root.findViewById(R.id.frame_img);
        pic = (RoundedNetImage) frameImg.findViewById(R.id.pic);
        name = (EditText) root.findViewById(R.id.name);
        userName = (EditText) root.findViewById(R.id.user_name);
        email = (EditText) root.findViewById(R.id.email);
        password = (EditText) root.findViewById(R.id.password);
        male = (RadioButton) root.findViewById(R.id.male);
        female = (RadioButton) root.findViewById(R.id.female);
        linearSend = (LinearLayout) root.findViewById(R.id.linear_send);
        send = (Button) linearSend.findViewById(R.id.send);

        send.setOnClickListener(this);

        socialNetwork = Fragment_SignIn.mSocialNetworkManager.getSocialNetwork(networkId);
        socialNetwork.setOnRequestCurrentPersonCompleteListener(this);
        socialNetwork.requestCurrentPerson();
    }

    @Override
    public void onClick(View v) {
        if (v == send) {
            String na = name.getText().toString();
            String userN = userName.getText().toString();
            String e = email.getText().toString();
            String pass = password.getText().toString();
            String gender = "male";
            if (female.isChecked()) {
                gender = "female";
            }
            if (na.length() > 0 && userN.length() > 0 && e.length() > 0 && pass.length() > 0) {

                postSignUp(na, userN, e, pass, gender, img);
            } else {
                Toast.makeText(getActivity(), getString(R.string.fields), Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        db.close();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DB(getActivity());
        try {
            db.open();
        } catch (SQLDataException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_complete_sign_up, container, false);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("please wait");
        findViews(view);
        return view;
    }

    @Override
    public void onRequestSocialPersonSuccess(int i, SocialPerson socialPerson) {
        img = socialPerson.avatarURL;
        name.setText(socialPerson.name);
        email.setText(socialPerson.email);
        ImageLoader mImageLoader = MySingleton.getInstance(getActivity()).getImageLoader();
        pic.setImageUrl(img, mImageLoader);

    }

    @Override
    public void onError(int networkId, String requestID, String errorMessage, Object data) {
        Toast.makeText(getActivity(), "ERROR: " + errorMessage, Toast.LENGTH_LONG).show();
    }

    public void postSignUp(final String name, final String userName, final String email, final String password,
                           String gender, String image) {
        final Map<String, String> params = new HashMap<>();
        try {
            params.put("image", image);
            params.put("name", name);
            params.put("username", userName);
            params.put("email", email);
            params.put("password", password);
            params.put("gender", gender);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String sentToken = sharedPreferences.getString(Fragment_SignIn.GCM_REGISTRATION_ID, "null");
            params.put(Fragment_SignIn.GCM_REGISTRATION_ID, sentToken);
        } catch (Exception e) {
            e.getMessage();
        }
        showpDialog();
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL, new JSONObject(params)
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.w("RES", response + "");
                    Log.e("in", "");
                    Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();
                    Log.w("RESUCCESS", response + "");
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equals("success")) {
                        JSONObject user = response.getJSONObject("user");
                        int id = user.getInt("id");
                        String username = user.getString("username");
                        String email = user.getString("email");
                        String name = user.getString("name");
                        String dob = user.getString("dob");
                        String gender = user.getString("gender");
                        String image = user.getString("image");
                        String token = user.getString("access_token");
                        User_ user1 = new User_(id, username, email, name, dob, gender, image);

                        db.deleteUSER(user1.getId());
                        db.insertUSER(user1.getId(), user1.getName(), user1.getUsername(), user1.getEmail(), user1.getDob(),
                                user1.getGender(), user1.getImage());

                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(Fragment_SignIn.MY_PREFS_NAME,
                                Context.MODE_PRIVATE).edit();
                        editor.putInt(Fragment_SignIn.USER_ID, user1.getId());
                        editor.putString(Fragment_SignIn.ACCESS_TOKEN, token);
                        editor.apply();

                        hidepDialog();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    } else
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    hidepDialog();
                } catch (Exception e) {
                    Log.e("out", "");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("first ERROR", error.getMessage() + ":");
                error.printStackTrace();
                Log.e("error", "");
                hidepDialog();
                Toast.makeText(getActivity(), "invalid", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        MySingleton.getInstance(getActivity()).addToRequestQueue(customRequest);
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
